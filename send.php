<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once('vendor/autoload.php');


function validateEmail($email) {
  return filter_var($email, FILTER_VALIDATE_EMAIL);
}

if (!empty($_POST) &&
  !empty($_POST['name']) &&
  !empty($_POST['email']) &&
  !empty($_POST['phone']) &&
  !empty($_POST['message']) &&
  validateEmail($_POST['email']))
{

  $mail = new PHPMailer;

  $mail->From = "info@wifix.us";
  $mail->FromName = "Wifix";

  $mail->addAddress("info@wifix.us", "Wifix");

  $mail->addReplyTo($_POST['email'], $_POST['name']);

  $mail->isHTML(true);

  $mail->Subject = "Consulta - Wifix";
  $mail->Body = "";
  $mail->Body .= "<h3>Message sent from Wifix</h3>";
  $mail->Body .= "<b>Name: </b>".$_POST['name']."<br>";
  $mail->Body .= "<b>Email: </b>".$_POST['email']."<br>";
  $mail->Body .= "<b>Phone: </b>".$_POST['phone']."<br>";
  $mail->Body .= "<b>Message: </b>".nl2br($_POST['message']);
  //$mail->AltBody = "This is the plain text version of the email content";

  if(!$mail->send())
  {
    error_log("Error sending this email: " . $mail->ErrorInfo);
    echo 0;
  }
  else
  {
    echo 1;
  }
}
else
{
  echo -1;
}