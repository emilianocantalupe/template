$(function()
{
  var jqxhr = false;

  function validateEmail(email) {
    var re = /^(([^<>()[\]\.,;:\s@"]+(\.[^<>()[\]\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  /**
   * Muestra un popup con un estado
   * @param  {string} msgTitle Título del mensaje
   * @param  {string} msgBody  Cuerpo del mensaje
   * @param  {string} msgType  Clase CSS del contenedor del mensaje
   */
  function showStatus(msgTitle,msgBody,msgType)
  {
    if (typeof msgTitle == 'undefined') msgTitle = '';
    if (typeof msgBody == 'undefined') msgBody = '';
    if (typeof msgType == 'undefined') msgType = '';

    $('#formStatus').removeClass().addClass('modal fade').addClass(msgType);

    $('#formStatus .modal-title').text(msgTitle);
    if (msgBody) $('#formStatus .modal-body').text(msgBody).show(); else $('#formStatus .modal-body').hide();

    $('#formStatus').modal('show');
  }
  function clearForm($form)
  {
    $form.find('input[type!=hidden],textarea,select').val('');
  }

  /**
   * Valida que el formulario "$form" tenga un nombre, email, telefono y mensaje completos y correctos.
   * @param  {jQuery Object Instance} $form
   * @return {boolean}       ['true' si es valido, o 'false' en caso contrario]
   */
  function validateForm($form)
  {
    var $name = $form.find('input[name="name"]');
    var $email = $form.find('input[name="email"]');
    var $phone = $form.find('input[name="phone"]');
    var $message = $form.find('textarea[name="message"]');

    $name.removeClass('error');
    $email.removeClass('error');
    $phone.removeClass('error');
    $message.removeClass('error');

    $('#nameError').removeClass('show');
    $('#emailError').removeClass('show');
    $('#message').removeClass('show');

    if (!$name.val()) {
      $name.addClass('error');
      $('#nameError').addClass('show');
    }

    if (!$email.val()) {
      $email.addClass('error');
      $('#emailError').addClass('show');
    }

    if (!$message.val()) {
      $message.addClass('error');
      $('#messageError').addClass('show');
    }

    if ($form.find('.error').length > 0) return false;

    else return true;
  }

  $('#formSuperior').submit(function()
  {
    var $form = $(this);
    if (!jqxhr && validateForm($form))
    {
      jqxhr = $.ajax({
        url: 'send.php',
        data:$form.serialize(),
        type:'POST',
        complete: function(){
          jqxhr = false;
        },
        success: function(msg){
          if (msg == 1)
          {
            showStatus('Message sent!','Your message has been sent correctly. Thank you for contacting us!','success');
          }
          else
          {
            if (msg == -1)
            {
              showStatus('We could not send your message', 'Please, complete the form correctly');
              clearForm($form);
            }
            else
            {
              showStatus('There was a problem sending your message','Please try agian in a few minutes.','error');
            }
          }

          $('#formStatus').modal('show');
        }
      });
    }

    return false; //Previene la ejecucion normal del formulario
  });
});